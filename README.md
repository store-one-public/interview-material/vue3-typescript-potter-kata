# Potter books kata using Typescript and Vue3

## Problem Description
Once upon a time there was a series of 5 books about a very famous English hero called Harry.
(At least when this Kata was invented, there were only 5. Since then they have multiplied)
Children all over the world thought he was fantastic, and, of course, so did the publisher.
So in a gesture of immense generosity to mankind, (and to increase sales) they set up the following
pricing model to take advantage of Harry’s magical powers.

One copy of any of the five books costs 8 EUR. If, however, you buy two different books from the series,
you get a 5% discount on those two books. If you buy 3 different books, you get a 10% discount.
With 4 different books, you get a 20% discount. If you go the whole hog, and buy all 5, you get a huge
25% discount.

Note that if you buy, say, four books, of which 3 are different titles, you get a 10% discount on the 3
that form part of a set, but the fourth book still costs 8 EUR.

Potter mania is sweeping the country and parents of teenagers everywhere are queueing up with shopping
baskets overflowing with Potter books. Your mission is to write a piece of code to calculate the price
of any conceivable shopping basket, giving as big a discount as possible.

For example, how much does this basket of books cost?

* 2 copies of the first book
* 2 copies of the second book
* 2 copies of the third book
* 1 copy of the fourth book
* 1 copy of the fifth book
answer :
```
(4 * 8) - 20% [first book, second book, third book, fourth book]
+ (4 * 8) - 20% [first book, second book, third book, fifth book]
  = 25.6 * 2
  = 51.20
```

## How to run this project
This is a vue project using Typescript and Vite.
For running the application, you need to download all dependencies:
```
$ npm install
```
Now you can run the application using:
```
$ npm run dev
  vite v2.4.1 dev server running at:

  > Local: http://localhost:3000/
  > Network: use `--host` to expose

  ready in 252ms.
```
You can navigate to [http://localhost:3000/](http://localhost:3000/) to
see the result.

## Clues
You’ll find that this Kata is easy at the start. You can get going with tests for baskets of 0 books,
1 book, 2 identical books, 2 different books… and it is not too difficult to work in small steps and
gradually introduce complexity.

However, the twist becomes apparent when you sit down and work out how much you think the sample basket
above should cost. It isn’t `5 * 8 * 0.75 + 3 * 8 * 0.90`. It is in fact `4 * 8 * 0.8 + 4 * 8 * 0.8`.
So the trick with this Kata is not that the acceptance test you’ve been given is wrong.
The trick is that you have to write some code that is intelligent enough to notice that two sets of four
books is cheaper than a set of five and a set of three.

You will have to introduce a certain amount of clever optimization algorithm. But not too much!
This problem does not require a fully fledged general purpose optimizer. Try to solve just this
problem well in order to share it for everyone or even in the ??? . Trust that you can generalize
and improve your solution if and when new requirements come along.

## Vue 3 + Typescript + Vite

### Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur). Make sure to enable `vetur.experimental.templateInterpolationService` in settings!

#### If Using `<script setup>`

[`<script setup>`](https://github.com/vuejs/rfcs/pull/227) is a feature that is currently in RFC stage. To get proper IDE support for the syntax, use [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) instead of Vetur (and disable Vetur).

### Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can use the following:

#### If Using Volar

Run `Volar: Switch TS Plugin on/off` from VSCode command palette.

#### If Using Vetur

1. Install and add `@vuedx/typescript-plugin-vue` to the [plugins section](https://www.typescriptlang.org/tsconfig#plugins) in `tsconfig.json`
2. Delete `src/shims-vue.d.ts` as it is no longer needed to provide module info to Typescript
3. Open `src/main.ts` in VSCode
4. Open the VSCode command palette
5. Search and run "Select TypeScript version" -> "Use workspace version"
